/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 17:40:08 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 17:40:10 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int		res;
	int		i;

	res = 1;
	i = 0;
	if (nb == 0)
		return (1);
	else if (nb > 0 && nb < 13)
	{
		while (i++ < nb)
			res *= i;
		return (res);
	}
	else
		return (0);
}
