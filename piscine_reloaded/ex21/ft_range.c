/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 17:51:00 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 17:51:02 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int			*tab;
	int			i;
	long int	size;

	i = 0;
	if (min >= max)
		return (NULL);
	size = (long)max - (long)min;
	if ((tab = malloc(sizeof(int) * size)) == NULL)
		return (NULL);
	while (i <= size)
	{
		tab[i] = min + i;
		i++;
	}
	return (tab);
}
