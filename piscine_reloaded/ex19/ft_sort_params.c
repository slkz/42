/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 17:48:27 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 17:48:28 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int		ft_strcmp(char *s1, char *s2)
{
	unsigned int	i;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] == s2[i])
			i++;
		else
			return ((unsigned char)s1[i] - (unsigned char)s2[i]);
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}

void	ft_swap_tab(char **a, char **b)
{
	char	*c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_sort_params(char **tab, int size)
{
	int		i;
	int		j;
	int		count;

	i = 1;
	j = 2;
	count = 1;
	while (count != 0)
	{
		count = 0;
		while (j < size)
		{
			if (ft_strcmp(tab[i], tab[j]) > 0)
			{
				ft_swap_tab(&tab[i], &tab[j]);
				count++;
			}
			i++;
			j++;
		}
		i = 1;
		j = 2;
	}
}

int		main(int argc, char **argv)
{
	if (argc == 1)
		return (0);
	ft_sort_params(argv, argc);
	argv++;
	while (*argv)
	{
		ft_putstr(*argv++);
		ft_putchar('\n');
	}
	return (0);
}
