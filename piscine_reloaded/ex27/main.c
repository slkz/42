/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 18:12:22 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 18:27:05 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>

int		main(int argc, char **argv)
{
	int		ret;
	int		fd;
	char	buf[51];

	ret = 42;
	if (argc < 2)
		write(2, "File name missing.\n", 19);
	else if (argc > 2)
		write(2, "Too many arguments.\n", 20);
	else
	{
		fd = open(argv[1], O_RDONLY);
		if (fd < 0)
			return (-1);
		while ((ret = read(fd, buf, 50)) > 0)
		{
			buf[ret] = '\0';
			write(1, buf, ret);
		}
	}
	return (0);
}
