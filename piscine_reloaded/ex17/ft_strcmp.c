/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 17:46:02 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 17:46:04 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(char *s1, char *s2)
{
	unsigned int	i;
	unsigned int	diff;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] == s2[i])
			i++;
		else
		{
			diff = (unsigned char)s1[i] - (unsigned char)s2[i];
			return (diff);
		}
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
