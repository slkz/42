/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 17:10:43 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/11 14:33:34 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*ddst;
	const char	*ssrc;

	ddst = dst;
	ssrc = src;
	if (src <= dst)
	{
		ssrc = ssrc + (len - 1);
		ddst = ddst + (len - 1);
		while (len--)
			*ddst-- = *ssrc--;
	}
	else
		ft_memcpy(dst, src, len);
	return (dst);
}
