/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:23:27 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/09 15:28:52 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	char	*clean;
	int		i;

	i = 0;
	str = (char *)s;
	if (str == NULL)
		return (NULL);
	if ((clean = malloc(sizeof(char) * ft_strlen(str) + 1)) == NULL)
		return (NULL);
	while (s[i] != '\0' && f)
	{
		clean[i] = f(s[i]);
		i++;
	}
	clean[i] = '\0';
	return (clean);
}
