/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 12:35:53 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/11 15:57:01 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char	*str;
	int		len;
	int		n_save;
	int		len_save;

	n_save = n;
	len = ft_dec_in_int(n);
	if (n == -2147483648)
		return (str = ft_strsub("-2147483648", 0, 11));
	n < 0 ? len += 1 : len;
	if ((str = malloc(sizeof(char) * len + 1)) == NULL)
		return (NULL);
	len_save = len;
	if (n < 0 && (n = -n))
		str[0] = '-';
	--len;
	while (len > 0)
	{
		str[len--] = ft_put_first_dtoc(n);
		n = n / 10;
	}
	if (n_save >= 0)
		str[len] = ft_put_first_dtoc(n);
	str[len_save] = '\0';
	return (str);
}
