/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_len_by_char.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:21:44 by lcuzzuco          #+#    #+#             */
/*   Updated: 2015/12/14 16:05:20 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_split_len_by_char(char *s, char c)
{
	int		i;
	int		splitted;

	if (!s || !c)
		return (0);
	i = 0;
	splitted = 0;
	while (s[i])
	{
		if (s[i] == c)
			splitted--;
		while (s[i] && s[i] != c)
			i++;
		splitted++;
		while (s[i] == c)
			i++;
	}
	return (splitted);
}
