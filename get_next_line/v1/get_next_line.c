/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 17:21:25 by lcuzzuco          #+#    #+#             */
/*   Updated: 2017/04/18 19:01:32 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft/libft.h"

void	clear_mem(char **ptr)
{
	free(*ptr);
	*ptr = NULL;
}

char	*read_and_add(int fd, t_config *config)
{
	char	*new;
	int		size;
	char	buf[BUFF_SIZE + 1];

	if ((config->ret = read(fd, buf, BUFF_SIZE)) == -1)
		return (NULL);
	buf[config->ret] = '\0';
	if (ft_strlen(config->string[fd]) == 0)
	{
		new = ft_strdup(buf);
		return (new);
	}
	size = ft_strlen(config->string[fd]) + ft_strlen(buf);
	if ((new = malloc(sizeof(char) * size + 1)) == NULL)
		return (NULL);
	new = ft_strcat(config->string[fd], buf);
	return (new);
}

int		get_line(int fd, t_config *config, char *endline, char **line)
{
	if ((endline = ft_strchr(config->string[fd], '\n')) == NULL)
	{
		if (ft_strlen(config->string[fd]))
		{
			*line = ft_strdup(config->string[fd]);
			config->string[fd] = ft_strdup("");
			return (1);
		}
		*line = NULL;
		clear_mem(&config->string[fd]);
		return (0);
	}
	*line = ft_strsub(config->string[fd], 0, endline - config->string[fd]);
	if (ft_strlen(++endline))
		config->string[fd] = ft_strdup(endline);
	if (!ft_strlen(endline))
		config->string[fd] = ft_strdup("");
	return (1);
}

int		init_tab(int fd, t_config *config)
{
	int	i;

	i = 0;
	if (config->init != 1)
	{
		if ((config->string = malloc(sizeof(char *) * MAX_FD + 1)) == NULL)
			return (-1);
		config->init = 1;
		while (i < MAX_FD)
			config->string[i++] = NULL;
	}
	if (config->string[fd] == NULL)
		config->string[fd] = ft_strdup("");
	return (0);
}

int		get_next_line(int fd, char **line)
{
	static t_config		config = {42, NULL, 0};
	char				*endline;

	if (fd < 0 || fd > MAX_FD || line == NULL)
		return (-1);
	endline = NULL;
	if (init_tab(fd, &config) == -1)
		return (-1);
	while ((endline = ft_strchr(config.string[fd], '\n')) == NULL
		&& config.ret != 0)
		if ((config.string[fd] = read_and_add(fd, &config)) == NULL)
		{
			clear_mem(&config.string[fd]);
			return (-1);
		}
	return (get_line(fd, &config, endline, line));
}
