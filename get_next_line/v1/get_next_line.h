/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 17:18:29 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/11/02 23:20:48 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 32
# define MAX_FD 1024

typedef struct		s_config
{
	int				ret;
	char			**string;
	int				init;
}					t_config;

int					get_next_line(int fd, char **line);

#endif
