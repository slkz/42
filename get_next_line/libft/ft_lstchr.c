#include "libft.h"

t_list	*ft_lstchr(t_list **list, char c)
{
	t_list	*ptr;

	if (!*list)
		return (NULL);
	ptr = *list;
	while (ptr)
	{
		if (ft_strchr(ptr->content, c))
			return (ptr);
		ptr = ptr->next;
	}
	return (NULL);
}
