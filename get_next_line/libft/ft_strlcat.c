/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 14:14:20 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/11 15:38:44 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	lendst;
	size_t	lensrc;

	lendst = ft_strlen(dst);
	lensrc = ft_strlen((char *)src);
	i = 0;
	if (size > lendst + 1)
	{
		while (i < (size - lendst - 1))
		{
			dst[i + lendst] = src[i];
			i++;
		}
		dst[lendst + i] = '\0';
	}
	if (size >= lendst)
		return (lendst + lensrc);
	return (lensrc + size);
}
