/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:16:47 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/11 15:24:07 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		sign;
	int		nb;

	sign = 1;
	nb = 0;
	while (*str == '\r' || *str == ' ' || *str == '\t' || *str == '\n'
			|| *str == '\v' || *str == '\f')
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			sign = -1;
		str++;
	}
	while (ft_isdigit(*str) == 1)
	{
		if ((ft_strcmp("-2147483648", str)) == 0)
			return (nb = -2147483648);
		nb = nb * 10 + *str - '0';
		str++;
	}
	nb *= sign;
	return (nb);
}
