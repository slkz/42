/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_pre_spaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:21:14 by lcuzzuco          #+#    #+#             */
/*   Updated: 2015/12/14 19:03:42 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	ft_search_pre_spaces(char *str)
{
	unsigned int	i;
	unsigned int	count;

	i = 0;
	count = 0;
	if (ft_isspace(str[i]) == 1)
	{
		while (ft_isspace(str[i]) == 1)
		{
			count++;
			i++;
		}
	}
	return (count);
}
