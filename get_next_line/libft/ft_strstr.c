/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 13:41:58 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/07 19:05:12 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	if (!*little)
		return ((char *)big);
	while (*big)
	{
		if (!ft_strncmp(big, little, ft_strlen((char *)little)))
			return ((char *)big);
		big++;
	}
	return (NULL);
}
