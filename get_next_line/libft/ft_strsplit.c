/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:25:07 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/05 16:13:29 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(const char *s, char c)
{
	char	**splitted;
	int		index;
	int		size;

	index = 0;
	size = ft_split_len_by_char((char *)s, c);
	if ((splitted = (char **)malloc(sizeof(char *) * size + 1)) == NULL)
		return (NULL);
	while (size--)
	{
		while (*s == c)
			s++;
		splitted[index] = ft_strsub((char *)s, 0, ft_wordlen((char *)s, 0, c));
		if (splitted[index] == NULL)
			return (NULL);
		s = s + ft_wordlen((char *)s, 0, c);
		index++;
	}
	splitted[index] = NULL;
	return (splitted);
}
