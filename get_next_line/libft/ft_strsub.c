/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:25:29 by lcuzzuco          #+#    #+#             */
/*   Updated: 2015/12/14 17:00:46 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char			*new;
	size_t			count;
	unsigned int	index;

	if (!s)
		return (0);
	index = start;
	count = 0;
	new = malloc(sizeof(char) * (len + 1));
	if (!new)
		return (NULL);
	while (count < len)
	{
		new[count] = s[index];
		index++;
		count++;
	}
	new[count] = '\0';
	return (new);
}
