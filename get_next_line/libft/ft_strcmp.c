/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 13:38:52 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/05 12:02:01 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *s1, const char *s2)
{
	int				i;
	unsigned int	diff;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] == s2[i])
			i++;
		else
		{
			diff = (unsigned char)s1[i] - (unsigned char)s2[i];
			return (diff);
		}
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
