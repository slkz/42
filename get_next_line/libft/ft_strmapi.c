/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:23:38 by lcuzzuco          #+#    #+#             */
/*   Updated: 2016/01/11 15:43:19 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*str;
	char			*clean;
	unsigned int	i;

	i = 0;
	str = (char *)s;
	if (str == NULL)
		return (NULL);
	if ((clean = malloc(sizeof(char) * ft_strlen(str) + 1)) == NULL)
		return (NULL);
	while (s[i] && f)
	{
		clean[i] = f(i, s[i]);
		i++;
	}
	clean[i] = '\0';
	return (clean);
}
