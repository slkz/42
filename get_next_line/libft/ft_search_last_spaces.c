/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_last_spaces.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 16:20:39 by lcuzzuco          #+#    #+#             */
/*   Updated: 2015/12/07 16:20:59 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int		ft_search_last_spaces(char *str)
{
	unsigned int	count;
	int				len;

	len = ft_strlen(str) - 1;
	count = 0;
	if (ft_isspace(str[len]) == 0)
		return (0);
	while (len != 0)
	{
		if (count == 0 && len != 0)
		{
			while (len >= 0 && ft_isspace(str[len]) != 0)
			{
				count++;
				len--;
			}
			return (count);
		}
	}
	return (count);
}
