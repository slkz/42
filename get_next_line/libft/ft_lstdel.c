/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel_incomplete.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcuzzuco <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 17:14:47 by lcuzzuco          #+#    #+#             */
/*   Updated: 2015/12/14 17:37:42 by lcuzzuco         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*ptr;
	t_list	*nextlst;

	ptr = *alst;
	while (ptr)
	{
		nextlst = ptr->next;
		ft_lstdelone(&ptr, del);
		ptr = nextlst;
	}
	*alst = NULL;
}
