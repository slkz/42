#include "get_next_line.h"
#include "libft/libft.h"

char	*get_line(t_list **begin_list, t_list *newline)
{
	t_list	*ptr;
	size_t	size;
	char	*line;

	size = 0;
	if ((ptr = *begin_list) == NULL)
		return (NULL);
	while (ptr && ptr != newline)
	{
		size += ptr->content_size - 1;
		ptr = ptr->next;
	}
	if ((line = ft_strnew(size)) == NULL)
		return (NULL);
	while (ptr && ptr != newline)
	{
		line = ft_strcat(line, (char *)ptr->content);
		ptr = ptr->next;
	}
	if (ptr)
		line = ft_strcat(
}

int		get_next_line(int fd, char **line)
{
	static t_list	*list[MAX_FD] = {0};
	t_list			*newline;
	int				ret;
	char			*buf;

	if (fd < 0 || fd > MAX_FD || !line || (buf = ft_strnew(BUFF_SIZE)) == NULL)
		return (-1);
	while ((newline  = ft_lstchr(&list[fd], '\n')) == NULL
			&& (ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		ft_list_push_back(&list[fd], (void *)buf, BUFF_SIZE + 1);
	}
	*line = get_line(&list[fd], newline);
}
